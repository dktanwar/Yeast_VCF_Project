#!/bin/bash
SNPEFF="java -Xmx4g -jar /home/deepak/snpEff/snpEff/snpEff.jar"
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/D784G_filtered.vcf | lbzip2 -c > ~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/D784G.eff.vcf.bz2 && \
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/DAS217_filtered.vcf | lbzip2 -c > ~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/DAS217.eff.vcf.bz2 && \
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/dst1_filtered.vcf | lbzip2 -c > ~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/dst1.eff.vcf.bz2 && \
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/E1224G_filtered.vcf |lbzip2 -c > ~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/E1224G.eff.vcf.bz2 && \
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/F1205H_filtered.vcf | lbzip2 -c >~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/F1205H.eff.vcf.bz2 && \
$SNPEFF -v SacCer_Apr2011.21 ~/Yeast_VCF_Project/data/rpa12_filtered.vcf | lbzip2 -c > ~/Yeast_VCF_Project/results/01_annotation_VCF_snpEff/rpa12.eff.vcf.bz2




