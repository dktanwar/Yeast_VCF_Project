#!/usr/bin/python

from sys import argv
input = argv[1]
file = open(input, 'r')

mydict = {}

for line in file.readlines():
    if (line[0] != '#'):
        mylist = line.rstrip().split()
        if  (len(mylist) < 4):
            continue

        key = mylist[2]
        value = mylist[3]
 
        if (mydict.has_key(key) == False):
            mydict[key] = [value]
        else:
            mydict[key].append(value)

from tabulate import tabulate
print tabulate({"synonymous_variant":mydict["synonymous_variant"],"missense_variant": mydict["missense_variant"]}, headers="keys")
